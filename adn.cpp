/* Question 5 */
#include "CLHEP/Random/MTwistEngine.h"
#include <vector>
#include <cmath>

std::string getNucleic(double n);
int generateWord(CLHEP::MTwistEngine * s, int n);

int main() {
  CLHEP::MTwistEngine * s = new CLHEP::MTwistEngine();
  std::vector<int> v;

  int simu = 10;
  int n = 40;
  int moyenne = 0;
  int ecart = 0;

  for(int j=0; j<simu; ++j) {
    moyenne = 0;
    ecart = 0;
    for(int i=0; i<n; ++i) {
      // Simulation avec status
      std::string name = "./status/status-adn" + std::to_string(j*40 + i);
      s->restoreStatus(name.c_str());
      moyenne += generateWord(s, i+1);
    }
    moyenne = moyenne/n;
    v.push_back(moyenne);
    std::cout << "simulation " << std::to_string(j+1) << " moyenne : " << moyenne << std::endl;
  }

  moyenne = 0;
  for(auto i : v) {
    moyenne += i;
  }
  moyenne = moyenne/simu;

  for(auto i : v) {
    ecart += pow(i - moyenne, 2);
  }
  ecart = sqrt(ecart/simu);

  std::cout << "moyenne : " << moyenne << std::endl;
  std::cout << "ecart type : " << ecart << std::endl;

  return 0;
}

std::string getNucleic(double n) {
  if (n < 0.25) {
    return "A";
  }
  else if (n > 0.25 && n < 0.5) {
    return "C";
  }
  else if (n > 0.5 && n < 0.75) {
    return "G";
  }
  else if (n > 0.75 && n < 1) {
    return "T";
  }
}

int generateWord(CLHEP::MTwistEngine * s, int n) {
  int attempts = 0;
  std::string word;
  do {
    attempts++;
    word = "";
    for (int i=0; i<7; ++i) {
      word += getNucleic(s->flat());
    }
  }
  while(word != "GATTACA");
  //std::cout << "essais n°" << n << " : " << attempts << std::endl;
  return attempts;
}
