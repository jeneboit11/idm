/* Question 2 Sauvegarde et Restauration de status */
#include <iostream>
#include <cstring>
#include <string>
#include "CLHEP/Random/MTwistEngine.h"

void saveStatus(std::string, int, int);
void restoreStatus(std::string, int);

int main() {
	// Question 1
	//saveStatus("status", 10, 10);
	//restoreStatus("status1", 10);

	// Question 2
	saveStatus("./status/status-adn", 40*10, 1000000);

	return 0;
}

/* Fonction qui sauvegarde des status
 * name: nom du fichier (préfix)
 * status : nombre de status à sauvegarder
 * step : nombre de tirage par status
 * */
void saveStatus(std::string name, int status, int step) {
	CLHEP::MTwistEngine * s = new CLHEP::MTwistEngine();
	char fileName[30];

	for(int i=0; i<status; ++i) {
	  sprintf(fileName,"%s%d", name.c_str(), i);
		s->saveStatus(fileName);

		for(int j=0; j<step; ++j) {
			auto n = s->flat();
			if (j == 0) {
				std::cout << "status " << i << ": " << n << std::endl;
			}
		}
	}
}

/* Fonction qui restaure un status
 * fileName: nom du fichier
 * step: nombre de tirage
*/
void restoreStatus(std::string fileName, int step) {
	CLHEP::MTwistEngine * s = new CLHEP::MTwistEngine();
	s->restoreStatus(fileName.c_str());
	for(int i=0; i<step; ++i) {
		auto n = s->flat();
		if (i == 0) {
			std::cout << "status " << i << ": " << n << std::endl;
		}
	}
}
