/* Question 3, 4 Calcule de PI avec MC */
#include "CLHEP/Random/MTwistEngine.h"
#include <vector>
#include <thread>
#include <sstream>
#include <fstream>
#include <mutex>

void monteCarlo(std::string, int);
std::mutex mutex;

int main() {
	// Question 3
	//monteCarlo("./status/status", 0);

	// Question 4
	std::vector<std::thread> threads;
	for (int i=0; i<10; ++i) {
		threads.push_back(std::thread(monteCarlo, "./status/status", i));
	}
	for (std::thread & th : threads) {
		if (th.joinable())
			th.join();
	}

	return 0;
}

void monteCarlo(std::string fileName, int indice) {
	CLHEP::MTwistEngine * s = new CLHEP::MTwistEngine();
	fileName = fileName + std::to_string(indice);
	s->restoreStatus(fileName.c_str());

	int inner, total;
	int i, j;
	inner = 0, total = 0;

	for(j=0; j<2000000; j++) {
		auto x = s->flat();
		auto y = s->flat();
		if (sqrt(pow(x,2)+pow(y,2)) <= 1) {
			inner++;
		}
		total++;
	}

	float pi = ((float)inner/(float)total)*4;

	// Question 3
	//std::cout << "Calcul PI = " << pi << std::endl;

	// Question 4
	std::ofstream file;
	std::ostringstream oss;
	oss << "Calcul PI status" << indice << " = " << pi << std::endl;

	mutex.lock();
	file.open("reduceFile.txt", std::fstream::in | std::fstream::out | std::fstream::app);
	file << oss.str();
	file.close();
	mutex.unlock();
}
